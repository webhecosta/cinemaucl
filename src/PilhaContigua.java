public class PilhaContigua {
    /*-----------------------------------
    ATRIBUTOS DA CLASSE
    -----------------------------------*/
    private Elemento Pilha[];
    int topo;
    /*-----------------------------------
    CONSTRUTOR DA CLASSE
    -----------------------------------*/
    public PilhaContigua() {
    }
    /*-----------------------------------
    MÉTODOS get E set DA CLASSE
    -----------------------------------*/
    // Método para acessar o topo da pilha.
    public int getTopo() {
        return topo;
    }
    /*-----------------------------------
    OPERAÇÕES DO TAD PILHA C
    -----------------------------------*/
    // Método para criar a pilha
    public void criarPilha(int tamanhoDaPilha) {
        this.topo = -1;
        Pilha = new Elemento[tamanhoDaPilha];
    }
    // Método para verificar se a pilha está cheia.
    public boolean isFull() {
        return topo == (Pilha.length - 1);
    }
    // Método para verificar se a pilha está vazia.
    public boolean isEmpty() {
        return topo == -1;
    }
    // Método para empilhar dados na pilha.
    public boolean push(int sala) {
        // Se a pilha não estiver cheia,
        if (!isFull()) {
            topo = topo + 1; // Atualiza o topo
            // Cria um novo nó, insere os dados da sala
            Elemento novoNo = new Elemento(sala);
            // Insere o nó criado na posição topo.
            Pilha[topo] = novoNo;
            return true;
        }
        return false;
    }
    // Método para desempilhar dados da pilha.
    public int pop() {
        // Tenta acessar o topo da pilha.
        int s = top();
        // Se o resultado for diferente de vazio, atualiza o topo.
        if (s != -1)
            topo = topo - 1;
        // Retorna a string (Se tiver elemento na pilha, o retorna,
        // caso contrário retorna vazio).
        return s;
    }
    // Método para apresentar os dados de quem está no topo
    // (se a pilha estiver com pelo menos 1 elemento)
    public int top() {
        // Se a pilha não estiver vazia, apresenta os dados que estão no topo.
        if (!isEmpty())
            return Pilha[topo].retornaSala();
        return -1;
    }
    // MOSTRAR A QUANTIDADE DE NÓ DA PILHA
    public int size() {
        // SE O TOPO FOR IGUAL A ZERO, A PILHA ESTÁ VAZIA,CASO CONTRÁRIO VAI RETORNAR O
        // TAMANHO DA PILHA
        if (topo == 0) {
            return 0;
        }
        return (topo + 1);
    }
    // ROMOVER O NÓ DA PILHA
    public void clear() {
        // Se a pilha não estiver vazia, atualiza o valor de 'topo'.
        if (!isEmpty())
            topo = -1;
    }
    // EXIBIR TODOS OS VALORES DA PILHA
    public void print() {
        if (!this.isEmpty()) {
            // Percorre do topo até a posição 0.
            for (int i = this.getTopo(); i >= 0; i--)
                System.out.println(Pilha[i].retornaSala());

        } else {
            System.out.println("Pilha vazia.");
        }
    }
}