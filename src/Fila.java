public class Fila {

    private Elemento primeiro;
    private Elemento ultimo;
    private int tamanho;

    // CONSTRUTOR VAZIO PARA INICIAR LISTA
    public Fila() {
        inicioFila();
    }

    // INICIARLIZAR LISTA
    public void inicioFila() {
        this.primeiro = null;
        this.ultimo = null;
        this.tamanho = 0;
    }

    // VERIRIFCAR SE A FILA ESTÁ VAZIA
    public boolean isEmpty() {

        return this.tamanho == 0;
    }

    // RETORNAR O PRIMEIRO DA FILA EM INTEIRO
    public int primeiroDaFila() {
        return primeiro.retornaSala();
    }

    // RETORNAR O PRIMEIRO ELEMENTO
    public Elemento getPrimeiro() {
        return this.primeiro;
    }

    public void setPrimeiro(Elemento primeiro) {
        this.primeiro = primeiro;
    }

    public Elemento getUltimo() {
        return this.ultimo;
    }

    public void setUltimo(Elemento ultimo) {
        this.ultimo = ultimo;
    }

    public void setTamanho(int tamanho) {
        this.tamanho = tamanho;
    }

    // ADICIONAR AS PESSOAS DENTRO DA FILA
    public void adicionarFila(String no, String em, String tel) {
        Elemento novoElemento = new Elemento(no, em, tel);
        if (primeiro == null && ultimo == null) {
            this.primeiro = novoElemento;
            this.ultimo = novoElemento;

        } else {
            this.ultimo.setProximo(novoElemento);
            this.ultimo = novoElemento;
        }
        this.tamanho++;
    }

    // REMOVER AS PESSOAS DA FILA PELO VALOR PROCURADO
    public void removerValor(String valorProcurado) {
        Elemento anterior = null;
        Elemento atual = this.primeiro;
        for (int i = 0; i < this.getTamanho(); i++) {

            if (atual.getValor().equals(valorProcurado)) { // COMPARAR SE O VALOR ENCONTRADO É O DESEJADO
                if (this.tamanho == 1) {
                    this.primeiro = null;
                    this.ultimo = null;
                } else if (atual == primeiro) {// VERIFICAR SE É O PRIMEIRO ITEM DA LISTA
                    this.primeiro = atual.getProximo();
                    atual.setProximo(null);
                } else if (atual == ultimo) {
                    this.ultimo = anterior;
                    anterior.setProximo(null);
                } else {
                    anterior.setProximo(atual.getProximo()); // AO REMOVER O ITEM, TEM QUE PEGAR O PRÓXIMO APÓS O ITEM
                                                             // REMOVIDO
                    atual = null;
                }
                this.tamanho--;// DIMINUIR O TAMANHO DA LISTA
                break;
            }
            anterior = atual; // ITEM ANTERIOR DA LISTA
            atual = atual.getProximo(); // PRÓXIMO ITEM DA LISTA
        }

    }

    // MÉTODO PARA REMOVER UM NÓ DA FILA
    public boolean remover(int p) {

        // Se a lista estiver vazia ou
        // Se posição negativa ou
        // Se posição maior ou igual a quantidade de nós

        if (isEmpty() || (p < 0) || (p >= this.tamanho)) {

            return false; // Não é possível remover nós da lista.

        } else {

            // Caso 1: existe apenas 1 elemento.
            if (this.tamanho == 1) {

                inicioFila(); // Inicializa os valores do descritor da lista.

            } else {
                // Atributo responsável por percorrer a lista encadeada.
                // 'referenciaAtual' recebe uma cópia do endereço do nó
                // que está na 1ª posição da lista.
                Elemento referenciaAtual = this.primeiro;
                // Caso 2: posição == 0
                if (p == 0) {
                    // O PRIMEIRO VIRA O SEGUNDO DA FILA

                    this.primeiro = (referenciaAtual.getProximo());

                } else {
                    // Sentinela usado para percorrer até a posição desejada.
                    int i = 0;
                    // Enquanto não chegar na posição anterior a que se
                    // quer remover o nó, vai percorrendo a lista.
                    while (i < (p - 1)) {
                        referenciaAtual = referenciaAtual.getProximo();
                        i++;
                    }
                    // Quando chegar na posição desejada, verificar se caso:
                    // 3 ou 4.

                    // Caso 3: remover o nó que está na última posição da lista.
                    if (p == (tamanho - 1)) {
                        // Diz que o nó atual não terá mais acesso ao endereço
                        // do último nó da lista.
                        referenciaAtual.setProximo(null);
                        // O último nó da lista passa a ser
                        // o nó anterior ao que foi removido.
                        ultimo.setProximo(referenciaAtual);

                    } else {
                        // Caso 4: remover quem está entre 0 e p.
                        // Copia o endereço do nó a ser removido.
                        Elemento proximaReferencia = referenciaAtual.getProximo();

                        // Registra o endereço do nó posterior ao nó a
                        // ser removido como endereço do nó atual (nó anterior
                        // ao que será removido.
                        referenciaAtual.setProximo(proximaReferencia.getProximo());

                        // O nó a ser removido não tem mais acesso ao
                        // endereço do próximo nó.
                        proximaReferencia.setProximo(null);

                    }
                }

            }
            // Atualiza a quantidade de nós da lista.
            this.tamanho--;
            return true;
        }
    }

    // PEGAR O ELEMENTO CONFORME A POSIÇÃO
    public Elemento get(int posicao) {

        Elemento atual = this.primeiro;
        for (int i = 0; i < posicao; i++) {
            if (atual.getProximo() != null) {
                atual = atual.getProximo();
            }
        }

        return atual;
    }

    // PEGAR O TAMANHO DA FILA
    public int getTamanho() {
        return this.tamanho;
    }

}
