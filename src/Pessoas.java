public class Pessoas {

    private String nome;
    private String email;
    private int sala;

    // CONSTRUTOR VAZIO
    public Pessoas() {

    }

    // CONSTRUTOR COM OS DADOS DAS PESSOAS QUE SERÃO ADICIONADAS NA FILA
    public Pessoas(String n, int s, String e) {
        this.nome = n;
        this.sala = s;
        this.email = e;
    }

    // PEGAR O NOME
    public String getNome() {
        return this.nome;
    }

    // SETAR O NOME
    public void setNome(String nome) {
        this.nome = nome;
    }

    // PEGAR EMAIL
    public String getEmail() {
        return this.email;
    }

    // SETAR EMAIL
    public void setEmail(String email) {
        this.email = email;
    }

    // PEGAR SALA
    public int getSala() {
        return this.sala;
    }

    // SETAR SALA
    public void setSala(int sala) {
        this.sala = sala;
    }

    // ESTE MÉTODO VAI SETAR AS INFORMAÇÕES. TEM A MESMA CARACTERÍSTICA QUE O
    // CONSTRUTOR
    public void setInformações(String n, int s, String e) {
        this.nome = n;
        this.sala = s;
        this.email = e;
    }

}
