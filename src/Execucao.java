import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Execucao {

    public static void main(String[] args) throws Exception {
        //////////// INÍCIO DO SISTEMA/////////////////
        String resultado = "";
        // HH_mm_ss
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("ddMMyyyy");
        LocalDateTime now = LocalDateTime.now();
        Scanner ler = new Scanner(System.in);

        // EXIBINDO MENU INTERATIVO
        int opcao = 0;
        do {
            System.out.println("\n\n                                ### CINEMA UCL ###");
            System.out.println("\n                 =======================================================");
            System.out.println("                  |     1 - Processo Padrão                              |");
            System.out.println("                  |     2 - Limpar Fila                                  |");
            System.out.println("                  |     3 - Quantidade de Pessoas na fila                |");
            System.out.println("                  |     4 - Imprimir Lista de Agraciados entraram na sala|");
            System.out.println("                  |     5 - Limpar Lista                                 |");
            System.out.println("                  |     0 - Sair                                         |");
            System.out.println("                  ========================================================\n");

            System.out.println("Digite uma opção conforme Menu.");
            opcao = ler.nextInt();
            System.out.print("\n");

            switch (opcao) {
                case 0:
                    break;
                case 1:
                    // CHAMAR FUNÇÃO QUE EXECUTA O PROGRAMA DE FORMA PADRÃO
                    Processamento.processo();
                    break;
                case 2:
                    // RECUPERANDO A CLASSE FILA PARA PEGAR O VALOR ATUAL E LIMPAR
                    // SE A FILA FOR INSTANCIADA, E ESTIVER COM ALGUM DADO, O SISTEMA IRÁ LIMPAR
                    if (Processamento.fila != null) {
                        // SE A LISTA FOR INSTANCIADA, PORÉM ESTIVER ZERADA, VAI APRESENTAR ERRO DE
                        // LISTA VAZIA
                        if (Processamento.fila.getTamanho() != 0) {
                            Processamento.fila.inicioFila();
                            resultado += ("\n\n");
                            resultado += ("Limpando a Fila......" + "\n");
                            resultado += ("Tamanho da Fila : " + Processamento.fila.getTamanho() + "\n");
                        } else {
                            resultado += ("\n\n");
                            resultado += ("Erro... A fila já está vazia!" + "\n");
                        }
                        // SE A LISTA FOR INSTANCIADA, PORÉM ESTIVER VAZIA, VAI APRESENTAR ERRO DE LISTA
                        // VAZIA
                    } else {
                        resultado += ("\n\n");
                        resultado += ("Erro... A fila já está vazia!" + "\n");
                    }
                    System.out.println(resultado);
                    break;
                case 3:
                    // OBTENDO O TAMANHO ATUAL DA FILA
                    resultado += ("Tamanho da Fila :" + Processamento.fila.getTamanho() + "\n");
                    System.out.println(resultado);
                    break;
                case 4:

                    int auxInt = 0;
                    int auxFormat = 0;
                    String auxsrtFormat = "";
                    // ESTE FOR PEGA O TAMANHO DO TEXTO PARA COLOCAR O NOMES DENTRO DE UMA CAIXA DE
                    // FORMA CORRETA
                    for (int i = 0; i < (Processamento.lista.getTamanho()); i++) {
                        if (Processamento.lista.get(i).getNome().length() > auxInt) {
                            auxInt = Processamento.lista.get(i).getNome().length();
                        }
                    }
                    // COLOCAR O CABEÇALHO DA IMPRESSÃO
                    resultado += ("\n\n");
                    resultado += (" |        FOI SOLICITADO A EXIBIÇÃO DA LISTA     |" + "\n");
                    resultado += (" ------------------------------------------------" + "\n");
                    resultado += (" |           LISTA DE AGRACIADOS                 |" + "\n");
                    resultado += (" ------------------------------------------------" + "\n");
                    //
                    for (int i = 0; i < (Processamento.lista.getTamanho()); i++) {
                        auxFormat = auxInt - Processamento.lista.get(i).getNome().length();
                        auxsrtFormat = "";
                        for (int j = 0; j < auxFormat; j++) {
                            auxsrtFormat += " ";
                        }
                        auxsrtFormat += "|\n";
                        // NESTA VARIÁVEL SERÁ ARMAZENADO O TEXTO COM OS DEVIDOS ESPAÇOS
                        resultado += (" | NOME : " + Processamento.lista.get(i).getNome() + auxsrtFormat);
                    }
                    // EXIBIR A LISTA DE AGRACIADOS NA TELA
                    System.out.println(resultado);
                    break;
                case 5:
                    // LIMPAR A LISTA, CASO A LISTA NÃO TIVER SIDO INSTANCIADA, ELA VAI MOSTRAR ERRO
                    // DE LISTA VAZIA
                    if (Processamento.lista != null) {
                        // SE A LISTA FOR INSTANCIADA, E ESTIVER COM ALGUM DADO, O SISTEMA IRÁ LIMPAR
                        if (Processamento.lista.getTamanho() != 0) {
                            Processamento.lista.zerarLista();
                            resultado += ("\n\n");
                            resultado += (" O SISTEMA LIMPOU A LISTA" + "\n");
                            resultado += (" -----------------------" + "\n");
                            resultado += (" |      LISTA VAZIA    |" + "\n");
                            resultado += (" -----------------------" + "\n");
                            resultado += ("Tamanho Lista: " + Processamento.lista.getPrimeiro() + "\n");
                            // SE A LISTA FOR INSTANCIADA, PORÉM ESTIVER ZERADA, VAI APRESENTAR ERRO DE
                            // LISTA VAZIA
                        } else {
                            resultado += ("\n\n");
                            resultado += (" -----------------------------------" + "\n");
                            resultado += (" |  ERRO...A LISTA JÁ ESTÁ VAZIA    |" + "\n");
                            resultado += (" ------------------------------------" + "\n");
                        }
                        // SE A LISTA FOR INSTANCIADA, PORÉM ESTIVER VAZIA, VAI APRESENTAR ERRO DE LISTA
                        // VAZIA
                    } else {
                        resultado += ("\n\n");
                        resultado += (" -----------------------------------" + "\n");
                        resultado += (" |  ERRO...A LISTA JÁ ESTÁ VAZIA    |" + "\n");
                        resultado += (" ------------------------------------" + "\n");
                    }

                    System.out.println(resultado);
                    break;
                default:
                    System.out.println("Opção Inválida!");
                    break;
            }

        } while (opcao != 0);
        // EXIBIR CABEÇALHO NO FINAL DO LOG
        resultado += ("------------------------" + "\n");
        resultado += ("|FINALIZAÇÃO DO SISTEMA|" + "\n");
        resultado += ("------------------------" + "\n");
        Leituratxt.escritor("./src/Logs/log" + dtf.format(now) + ".txt", resultado); // GERANDO LOGS
        // CONFORME DATA E HORA
    }

}
