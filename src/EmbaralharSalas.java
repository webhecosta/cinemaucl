import java.util.Random;

public class EmbaralharSalas {
    // método estático que embaralha os elementos de um vetor de inteiros
    public static int[] embaralhar(int[] v) {

        // Random random = new Random();
        int idx1, idx2, idx3;
        int cont_v = v.length;
        int contidx3 = 0;
        int qtde = 0;

        // CRIAR O TAMANHO DO PRÓXIMO VETOR
        for (idx2 = 0; idx2 < cont_v; idx2++) {
            qtde = qtde + v[idx2];
        }
        // CRIAR UM VETOR PARA RECEBER OS INGRESSOS
        // O TAMANHO DO VETOR É O MESMO TAMANHO DA QUANTIDADE DE INGRESSOS
        int vetIngressosParaEmbaralhar[] = new int[qtde];
        // ESTE LAÇO POPULA O NOVO VETOR COM A QUANTIDADE DE INGRESSOS
        idx2 = 0;
        for (idx1 = 0; idx1 < qtde; idx1++) {
            for (idx2 = idx2; idx2 < cont_v; idx2++) {
                contidx3 = v[idx2];
                for (idx3 = 0; idx3 < contidx3; idx3++) {
                    vetIngressosParaEmbaralhar[idx1] = idx2;
                    idx1++;
                }
            }
        }
        // ESTE RETORNO, RETORNA PARA O LOCAL QUE A CHAMOU UM VETOR EMBARALHADO
        return embaralharArray(vetIngressosParaEmbaralhar);
    }

    public static int[] embaralharArray(int[] v) {

        Random random = new Random();

        for (int i = 0; i < (v.length - 1); i++) {
            // sorteia um índice
            int j = random.nextInt(v.length);
            // troca o conteúdo dos índices i e j do vetor
            int temp = v[i];
            v[i] = v[j];
            v[j] = temp;
        }
        return v;
    }
}
