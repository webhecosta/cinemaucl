import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

//FUNÇÃO PARA LER OS DADOS DENTRO DE UM TXT.
//DEVE SER PASSADO O CAMINHO DA PASTA QUE ESTÁ O ARQUIVO
public class Leituratxt {
    public static Fila leitor(String path) throws IOException {
        Fila fila = new Fila(); // LISTA GENÉRICA
        BufferedReader buffRead = new BufferedReader(new FileReader(path));
        String linha = "";
        int cont = 0; // CONTADOR
        while (true) {
            if (linha != null) {
                // SE FOR DIFERENTE DE VAZIO ELE VAI ENTRAR NO IF
                if (linha != "" && (cont <= 93)) {
                    String frase = linha;
                    String array[] = new String[3];
                    array = frase.split(";");
                    fila.adicionarFila(array[0], array[1], array[2]);// ADICIONANDO OS DADOS EM UMA FILA (FILA DE
                                                                     // PESSOAS DO CINEMA)
                }
            } else
                break;
            linha = buffRead.readLine();
            cont++;
        }
        buffRead.close();
        return fila;
    }

    // FUNÇÃO PARA ESCREVER OS DADOS DENTRO DE UM TXT.
    // DEVE SER PASSADO O CAMINHO DA PASTA E O TEXTO QUE DESEJA SER ARMAZENADO NO
    // LOG
    public static void escritor(String path, String msg) throws IOException {
        BufferedWriter buffWrite = new BufferedWriter(new FileWriter(path, true));
        String linha = "";
        linha = msg;
        System.out.println(linha);
        buffWrite.append(linha + "\n");
        buffWrite.close();
    }

}
