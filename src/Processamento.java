
import java.util.Scanner;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

public class Processamento {
        // CRIANDO UMA VARIÁVEL DO TIPO FILA PARA RECUPERAR O VALOR DENTRO DA CLASSE
        // EXECUÇÃO
        public static Fila fila;
        public static Lista lista;
        // public static PilhaContigua pilha;

        public static void processo() throws Exception {
                String resultado = "";
                // HH_mm_ss
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("ddMMyyyy");
                LocalDateTime now = LocalDateTime.now();
                Scanner ler = new Scanner(System.in);

                resultado += ("------------------------------------\n");
                resultado += ("OPÇÃO 1 - INÍCIANDO SISTEMA PADRÃO  \n");
                resultado += ("------------------------------------\n");

                resultado += ("------------------------------------\n");
                resultado += ("INSTANCIANDO A PILHA DE INGRESSOS\n");
                resultado += ("------------------------------------\n");

                // // INSTANCIAR PILHA

                PilhaContigua pilha = new PilhaContigua();
                int tamanhoFila = 0; // tamanho da fila
                int tamanhoPilha = 0; // tamanho da fila

                resultado += ("----------------------------------------------------------------------\n");
                resultado += ("CRIANDO VETOR E MÉTODO RANDOM PARA GERAR TOTAL DE CADEIRAS DE CADA\n"
                                + "SALA DO CINEMA DE FORMA ALEATÓRIA\n");
                resultado += ("----------------------------------------------------------------------\n");

                // DECLARANDO VETOR E UTILIZANDO O MÉTODO RANDOM PARA SORTEAR NÚMEROS
                Random gerador = new Random();
                int vetSala[] = new int[5];

                resultado += ("------------------------------------------------------------------------\n");
                resultado += ("CRIANDO VARIÁVEL DO TIPO FILA PARA OBTER O TAMANHO DA FILA\n"
                                + "E PARAR COMO PARÂMETRO O CAMINHO DO ARQUIVO TXT CONTENDO OS NOMES\n"
                                + "DOS USUÁRIOS DO CINEMA\n");
                resultado += ("------------------------------------------------------------------------\n");
                // VARIÁVEL DO TIPO FILA

              
                fila = (Leituratxt.leitor("./src/nomes.txt")); // LENDO O ARQUIVO TXT E ADICIONANDO OS INTEGRANTES
                                                               // NA FILA
                resultado += ("-----------------------------------------------------------" + "\n");
                resultado += ("PERCORRER E EXIBIR TODOS OS INTEGRANTES QUE ESTEJAM NA FILA" + "\n");
                resultado += ("-----------------------------------------------------------" + "\n");
                for (int i = 0; i < fila.getTamanho(); i++) {
                        resultado += (fila.get(i).getNome() + " - " + fila.get(i).getEmail() + " - "
                                        + fila.get(i).getTelefone() + "\n");
                }
                resultado += ("-----------------------------------------------------------" + "\n");
                resultado += ("TAMANHO ATUAL DA FILA" + "\n");
                resultado += ("-----------------------------------------------------------" + "\n");

                resultado += ("Tamanho : " + fila.getTamanho() + "\n");
                tamanhoFila = fila.getTamanho();
                resultado += ("-----------------------------------------------------------" + "\n");
                resultado += ("PRIMEIRO E ÚLTIMO NOME DA FILA" + "\n");
                resultado += ("-----------------------------------------------------------" + "\n");

                resultado += ("Primeiro valor da fila : " + fila.getPrimeiro().getNome() + "\n");
                resultado += ("Ultimo valor da fila : " + fila.getUltimo().getNome() + "\n");
                resultado += ("---------------------------------------" + "\n");
                resultado += ("CRIRANDO CADEIRAS NAS SALAS DE CINEMA" + "\n");
                resultado += ("---------------------------------------" + "\n");

                // // // CRIANDO A QUANTIDADE DE CADEIRAS POR SALA
                for (int i = 0; i < 5; i++) {
                        int randomNum = gerador.nextInt((15 - 10) + 1) + 10;
                        vetSala[i] = randomNum;
                }
                // // EXIBIR TOTAL DE CADEIRAS DENTRO DA SALA DE CINEMA
                resultado += ("-------------------------------------------------------" + "\n");
                resultado += ("EXIBINDO TOTAL DE CADEIRAS DENTRO DA SALA DE CINEMA.... " + "\n");
                resultado += ("--------------------------------------------------------" + "\n");
                for (int i = 0; i < 5; i++) {
                        resultado += ("SALA : " + vetSala[i] + "\n");
                }

                resultado += ("-------------------------------------------------------" + "\n");
                resultado += ("CRIANDO UM VETOR PARA RECEBER A LISTA DE INGRESSOS EMBARALHADOS.... " + "\n");
                resultado += ("--------------------------------------------------------" + "\n");
                int[] vetor = EmbaralharSalas.embaralhar(vetSala);

                resultado += ("----------------------------------" + "\n");
                resultado += ("EXIBIR INGRESSOS EMBARALHADOS.... " + "\n");
                resultado += ("-----------------------------------" + "\n");
                for (int i = 0; i < vetor.length; i++) {
                        resultado += ("INGRESSO : " + vetor[i] + "\n");
                }

                // // DETERMINAR O TAMANHO DA PILHA E INSERIR O NÚMERO DE INGRESSOS
                resultado += ("----------------------------" + "\n");
                resultado += ("CRIANDO O TAMANHO DA PILHA" + "\n");
                resultado += ("-----------------------------" + "\n");
                pilha.criarPilha(vetor.length);
                resultado += ("--------------------------------------------------------------------" + "\n");
                resultado += ("EXIBINDO TOPO E TAMANHO DA PILHA ANTES DE DISTRIBUIR OS INGRESSOS" + "\n");
                resultado += ("---------------------------------------------------------------------" + "\n");
                resultado += ("TAMANHO DA PILHA : " + pilha.size() + "\n"); // TAMANHO DA PILHA
                resultado += ("TOPO DA PILHA : " + pilha.top() + "\n");

                // // INSERIR INGRESSO NA PILHA
                resultado += ("----------------------------" + "\n");
                resultado += ("ADICIONANDO ITENS DENTRO DA PILHA" + "\n");
                resultado += ("-----------------------------" + "\n");
                for (int i = 0; i < vetor.length; i++) {
                        pilha.push(vetor[i]);
                        resultado += ("ITEM : " + vetor[i] + "\n");
                }
                tamanhoPilha = pilha.size();
                resultado += ("TAMANHO PILHA ANTES DE REMOVER : " + pilha.size() + "\n");
                // // INSTANCIAR LISTA
                resultado += ("----------------------------------------------------------------------------------------"
                                + "\n");
                resultado += ("INSTANCIANDO LISTA PARA ARMAZENAR OS DADOS DOS USUÁRIOS QUE ENTRARAM NA SALA DE CINEMA"
                                + "\n");
                resultado += ("-----------------------------------------------------------------------------------------"
                                + "\n");
                lista = new Lista();

                // //DISTRIBUIR OS INGRESSOS
                resultado += ("----------------------------------------------------------------------------------------"
                                + "\n");
                resultado += ("PERCORRENDO A FILA E ADICIONANDO NA LISTA OS DADOS DOS USUÁRIOS QUE ENTRARAM NO CINEMA"
                                + "\n");
                resultado += ("-----------------------------------------------------------------------------------------"
                                + "\n");

                for (int j = 0; j < tamanhoFila; j++) {
                        // INSERIR OS DADOS DA PESSOA QUE PEGOU O INGRESSO E ENTROU NA SALA DO CINEMA

                        if (j < tamanhoPilha) {

                                lista.adicionarLista(pilha.top(), fila.get(0).getNome(), fila.get(0).getEmail(),
                                                fila.get(0).getTelefone());
                                resultado += ("------------------------" + "\n");
                                resultado += ("REMOVENDO O INGRESSO " + pilha.top() + " DO TOPO DA PILHA\nREMOVENDO "
                                                + fila.get(0).getNome() + " DO INÍCIO DA FILA" + "\n");
                                resultado += ("-------------------------" + "\n");
                                pilha.pop(); // REMOVENDO E GERANDO NOVO TOPO
                                fila.remover(0);
                                resultado += ("NOVO TOPO DA PILHA : " + pilha.top() + "\n");
                                resultado += ("TAMANHO PILHA : " + pilha.size() + "\n");
                                resultado += ("TAMANHO FILA : " + fila.getTamanho() + "\n");

                        }
                }

                for (int h = 0; h < fila.getTamanho(); h++) {
                        resultado += ("NÃO É POSSÍVEL DISTRIBUIR INGRESSO PARA : " + fila.get(h).getNome()
                                        + ", POIS TODAS AS SALAS ESTÃO CHEIAS!" + "\n");
                }  

                resultado += ("-----------------------------------------------------------------------" + "\n");
                resultado += ("USUÁRIOS QUE NÃO ENTRARAM NO CINEMA POR FALTA DE VAGA!!!" + "\n");
                resultado += ("-----------------------------------------------------------------------" + "\n");
                for (int h = 0; h < fila.getTamanho(); h++) {

                        resultado += (fila.get(h).getNome() + " - " + fila.get(h).getEmail() + " - "
                                        + fila.get(h).getTelefone() + "\n");

                }

                // // PERCORRER LISTA POPULADA
                resultado += ("-----------------------------------------------------------------------" + "\n");
                resultado += ("PERCORRENDO A LISTA E EXIBINDO TODOS OS USUÁRIOS QUE ENTRARAM NO CINEMA" + "\n");
                resultado += ("-----------------------------------------------------------------------" + "\n");

                resultado += ("TAMANHO LISTA : " + lista.getTamanho() + "\n");
                for (int i = 0; i < lista.getTamanho(); i++) {
                        resultado += ("SALA : " + lista.get(i).getSala() + "- NOME : " + lista.get(i).getNome()
                                        + "- EMAIL : " + lista.get(i).getEmail() + "- TELEFONE : "
                                        + lista.get(i).getTelefone() + "\n");
                }
                // TAMANHO DA PILHA
                resultado += ("TAMANHO DA PILHA APÓS DISTRIBUIR INGRESSO : " + pilha.size() + "\n");
                resultado += ("NOVO TAMANHO FILA : " + fila.getTamanho() + "\n");
                // retornoFila(lista);
                Leituratxt.escritor("./src/Logs/log" + dtf.format(now) + ".txt", resultado); // GERANDO LOGS
                                                                                             // CONFORME DATA E HORA

        }    

}
