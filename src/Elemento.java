public class Elemento {

    private String valor;
    private String nome;
    private String email;
    private String telefone;
    private int sala;
    private Elemento proximo;

    // CONSTRUTOR VAZIO
    public Elemento() {
    }

    // CONSTRUTOR APENAS COM UMA PARAMETRO DO TIPO INTEIRO
    public Elemento(int sala) {
        this.sala = sala;
    }

    // CONSTRUTOR SEM SALA PARA POPULAR A FILA
    public Elemento(String nome, String email, String telefone) {
        this.nome = nome;
        this.email = email;
        this.telefone = telefone;
    }

    // CONSTRUTOR COM OS PARÂMETROS NECESSÁRIOS PARA POPULAR A LISTA
    public Elemento(int sala, String nome, String email, String telefone) {
        this.sala = sala;
        this.nome = nome;
        this.email = email;
        this.telefone = telefone;
    }

    // CONSTRUTOR COM APENAS UM PARÂMETRO
    public Elemento(String novoValor) {
        this.valor = novoValor;
    }

    // PEGAR O VALOR DO ELEMENTO
    public String getValor() {
        return this.valor;
    }

    // SETAR O VALOR DO ELEMENTO
    public void setValor(String valor) {
        this.valor = valor;
    }

    // PEGAR O NOME
    public String getNome() {
        return this.nome;
    }

    // SETAR O NOME
    public void setNome(String nome) {
        this.nome = nome;
    }

    // PEGAR EMAIL
    public String getEmail() {
        return this.email;
    }

    // SETAR EMAIL
    public void setEmail(String email) {
        this.email = email;
    }

    // PEGAR O NÚMERO DE TELEFONE
    public String getTelefone() {
        return this.telefone;
    }

    // SETAR O NÚMERO TELEFONE
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    // PEGAR A SALA
    public int getSala() {
        return this.sala;
    }

    // SETAR A SALA
    public void setSala(int sala) {
        this.sala = sala;
    }

    // PEGAR O PRÓXIMO ELEMENTO
    public Elemento getProximo() {
        return this.proximo;
    }

    // SETAR O PRÓXIMO ELEMENTO
    public void setProximo(Elemento proximo) {
        this.proximo = proximo;
    }

    // RETORNAR OS DADOS DA SALA EM STRING
    public String toString(int n) {
        return "[ Sala Cinema: " + this.sala + " ]";
    }

    // RETORNAR O NÚMERO DA SALA EM INTEIRO
    public int retornaSala() {
        return this.sala;
    }

}
