public class Lista {

    private Elemento primeiro;
    private Elemento ultimo;
    private int tamanho;

    // CONSTRUTOR VAZIO PARA INICIAR LISTA
    public Lista() {
        zerarLista();
    }
    //ZERAR LISTA
    public void zerarLista(){

        this.primeiro = null;
        this.ultimo = null;
        this.tamanho = 0;   
    }

    // ADICIONAR ITEM NO INICIO DA LISTA
    public void adicionarInicio(String novoValor) {
        Elemento novoElemento = new Elemento(novoValor);
        if (primeiro == null && ultimo == null) {
            this.primeiro = novoElemento;
            this.ultimo = novoElemento;

        } else {
            novoElemento.setProximo(this.primeiro);
            this.primeiro = novoElemento;
        }
        this.tamanho++;
    }

    public void adicionarFim(String novoValor) {
        Elemento novoElemento = new Elemento(novoValor);
        if (primeiro == null && ultimo == null) {
            this.primeiro = novoElemento;
            this.ultimo = novoElemento;

        } else {
            this.ultimo.setProximo(novoElemento);
            this.ultimo = novoElemento;
        }
        this.tamanho++;
    }

    // ADICIONAR LISTA COM NÚMERO DE SALA
    public void adicionarLista(int sa, String no, String em, String tel) {
        Elemento novoElemento = new Elemento(sa, no, em, tel);
        if (primeiro == null && ultimo == null) {
            this.primeiro = novoElemento;
            this.ultimo = novoElemento;

        } else {
            this.ultimo.setProximo(novoElemento);
            this.ultimo = novoElemento;
        }
        this.tamanho++;
    }

    // REMOVER PELO VALOR PROCURADO
    public void remover(String valorProcurado) throws Exception {
        Elemento anterior = null;
        Elemento atual = this.primeiro;
        for (int i = 0; i < this.getTamanho(); i++) {

            if (atual.getValor().equals(valorProcurado)) { // COMPARAR SE O VALOR ENCONTRADO É O DESEJADO
                if (this.tamanho == 1) {
                    this.primeiro = null;
                    this.ultimo = null;
                } else if (atual == primeiro) {// VERIFICAR SE É O PRIMEIRO ITEM DA LISTA
                    this.primeiro = atual.getProximo();
                    atual.setProximo(null);
                } else if (atual == ultimo) {
                    this.ultimo = anterior;
                    anterior.setProximo(null);
                } else {
                    anterior.setProximo(atual.getProximo()); // AO REMOVER O ITEM, TEM QUE PEGAR O PRÓXIMO APÓS O ITEM
                                                             // REMOVIDO
                    atual = null;
                }
                this.tamanho--;// DIMINUIR O TAMANHO DA LISTA
                break;
            }
            anterior = atual; // ITEM ANTERIOR DA LISTA
            atual = atual.getProximo(); // PRÓXIMO ITEM DA LISTA
        }

    }

    // EXIBIR O ELEMENTO PELA POSIÇÃO
    public Elemento get(int posicao) {

        Elemento atual = this.primeiro;
        for (int i = 0; i < posicao; i++) {
            if (atual.getProximo() != null) {
                atual = atual.getProximo();
            }
        }
        return atual;
    }

    // EXIBIR O PRIMEIRO ELEMENTO
    public Elemento getPrimeiro() {
        return this.primeiro;
    }

    // SETAR O PRIMEIRO
    public void setPrimeiro(Elemento primeiro) {
        this.primeiro = primeiro;
    }

    // PEGAR O ÚLTIMO
    public Elemento getUltimo() {
        return this.ultimo;
    }

    // SETAR O ÚLTIMO
    public void setUltimo(Elemento ultimo) {
        this.ultimo = ultimo;
    }

    // PEGAR O TAMANHO
    public int getTamanho() throws Exception {
        return this.tamanho;
    }

    // SETAR O TAMANHO
    public void setTamanho(int tamanho) {
        this.tamanho = tamanho;
    }
}
